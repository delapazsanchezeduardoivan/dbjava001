/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;
import javax.swing.JOptionPane;
/**
 *
 * @author delap
 */
public class TestDbProducto {
    public static void main(String[] args){
        
        dbProducto db = new dbProducto();
        Productos pro = new Productos();
        
        //Insertar.
        if(db.conectar()){
            System.out.println("Fue posible conectarse");
            
            pro.setCodigo("1000");
            pro.setNombre("Atun");
            pro.setPrecio(10.50f);
            pro.setStatus(0);
            
            try {
                db.insertar(pro);
                JOptionPane.showMessageDialog(null, "Se agrego con exito ");
                
            } catch(Exception e){
              JOptionPane.showMessageDialog(null, "Surgio un error " + e.getMessage()); 
            }
            // probar actualizar
            pro.setIdProductos(1);
            pro.setNombre("Jabon Ariel 1kg");
            pro.setCodigo("201");
            pro.setFecha("2024-06-25");
            pro.setPrecio((45.40f));
            
            try{
                db.actualizar(pro);
                JOptionPane.showMessageDialog(null, "Se Actualizo con exito");
            } catch(Exception e){
                JOptionPane.showMessageDialog(null,"Surgio un error " + e.getMessage());
            }
            
            //Probar habilitar.
            pro.setIdProductos(1);
            
            try{
                db.habilitar(pro);
                JOptionPane.showMessageDialog(null, "Se Habilito con exito");
            } catch(Exception e){
                JOptionPane.showMessageDialog(null,"Surgio un error " + e.getMessage());
            }
            //Probar deshabilitar.
            pro.setIdProductos(2);
            
            try{
                db.deshabilitar(pro);
                JOptionPane.showMessageDialog(null, "Se Deshabilito con exito");
            } catch(Exception e){
                JOptionPane.showMessageDialog(null,"Surgio un error " + e.getMessage());
            }
            //Probar siExiste 
            try{
                if(db.siExiste(1)) JOptionPane.showMessageDialog(null, "Si existe");
                else JOptionPane.showMessageDialog(null, "No existe");
            }catch (Exception e) {
                System.out.println("Surgio un error" + e.getMessage());
            }
            // Probar Buscar
            try{
            
            pro = (Productos) db.buscar("20");
            if(pro.getIdProductos() == 0){
                JOptionPane.showMessageDialog(null, "El producto no exite ");
            }
              else {
                JOptionPane.showMessageDialog(null, "Producto " + pro.getNombre() + "Precio " + pro.getPrecio());
            }
            }catch(Exception e){
                System.out.println("Surgio un error " + e.getMessage());
            }
            
        }
    }
    
}
